
export function createUUID() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
}

export class Eat {
  getanyeat() {
    let randid = this.getRandomId();
    return this.collection[randid];
  }
  getRandomId() {
    let min = 0;
    let max = this.collection.length - 1;
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  constructor() {
    this.collection = [
      "🍇 Grapes",
      "🍈 Melon",
      "🍉 Watermelon",
      "🍊 Tangerine",
      "🍋 Lemon",
      "🍌 Banana",
      "🍍 Pineapple",
      "🥭 Mango",
      "🍎 Red Apple",
      "🍏 Green Apple",
      "🍐 Pear",
      "🍑 Peach",
      "🍒 Cherries",
      "🍓 Strawberry",
      "🥝 Kiwi Fruit",
      "🍅 Tomato",
      "🥥 Coconut",
      "🥑 Avocado",
      "🍆 Eggplant",
      "🥔 Potato",
      "🥕 Carrot",
      "🌽 Ear of Corn",
      "🌶 Hot Pepper",
      "🥒 Cucumber",
      "🥬 Leafy Green",
      "🥦 Broccoli",
      "🍄 Mushroom",
      "🥜 Peanuts",
      "🌰 Chestnut",
      "🍞 Bread",
      "🥐 Croissant",
      "🥖 Baguette Bread",
      "🥨 Pretzel",
      "🥯 Bagel",
      "🥞 Pancakes",
      "🧀 Cheese Wedge",
      "🍖 Meat on Bone",
      "🍗 Poultry Leg",
      "🥩 Cut of Meat",
      "🥓 Bacon",
      "🍔 Hamburger",
      "🍟 French Fries",
      "🍕 Pizza",
      "🌭 Hot Dog",
      "🥪 Sandwich",
      "🌮 Taco",
      "🌯 Burrito",
      "🥙 Stuffed Flatbread",
      "🍳 Cooking",
      "🥘 Shallow Pan of Food",
      "🍲 Pot of Food",
      "🥣 Bowl With Spoon",
      "🥗 Green Salad",
      "🍿 Popcorn",
      "🧂 Salt",
      "🥫 Canned Food",
      "🍱 Bento Box",
      "🍘 Rice Cracker",
      "🍙 Rice Ball",
      "🍚 Cooked Rice",
      "🍛 Curry Rice",
      "🍜 Steaming Bowl",
      "🍝 Spaghetti",
      "🍠 Roasted Sweet Potato",
      "🍢 Oden",
      "🍣 Sushi",
      "🍤 Fried Shrimp",
      "🍥 Fish Cake With Swirl",
      "🥮 Moon Cake",
      "🍡 Dango",
      "🥟 Dumpling",
      "🥠 Fortune Cookie",
      "🥡 Takeout Box",
      "🍦 Soft Ice Cream",
      "🍧 Shaved Ice",
      "🍨 Ice Cream",
      "🍩 Doughnut",
      "🍪 Cookie",
      "🎂 Birthday Cake",
      "🍰 Shortcake",
      "🧁 Cupcake",
      "🥧 Pie",
      "🍫 Chocolate Bar",
      "🍬 Candy",
      "🍭 Lollipop",
      "🍮 Custard",
      "🍯 Honey Pot",
      "🍼 Baby Bottle",
      "🥛 Glass of Milk",
      "☕ Hot Beverage",
      "🍵 Teacup Without Handle",
      "🍸 Cocktail Glass",
      "🍹 Tropical Drink",
      "🥤 Cup With Straw",
      "🥢 Chopsticks",
      "🍽 Fork and Knife With Plate",
      "🍴 Fork and Knife",
      "🥄 Spoon"
    ];
  }
}